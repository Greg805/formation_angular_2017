import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../environments/environment';
import { AddComponent } from './items/containers/add/add.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found/page-not-found.component';
import { Routes, RouterModule } from '@angular/router';


const appRoutes: Routes = [


];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: []
})
export class AppRoutingModule {
 }
