import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  public isCollapsed = true;
  public title;
  //@Input('ngbCollapse') ngbCollapse = false;
  constructor() { }

  ngOnInit() {
    this.title = 'App Livreur (form ang)';
  }

}
