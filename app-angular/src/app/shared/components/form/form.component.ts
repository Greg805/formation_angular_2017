import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { State } from '../../enums/state.enum';
import { Item } from '../../interfaces/item.model';
import { COLLECTION } from '../../../items/collection';

import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';



@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  state = State;
  form: FormGroup;
  nameCtrl: FormControl;
  refCtrl: FormControl;
  stateCtrl: FormControl;

  @Output() dataItem: EventEmitter<Item> = new EventEmitter();

  constructor(private _FormBuilder: FormBuilder) {
    this.nameCtrl = this._FormBuilder.control('', [
      Validators.required,
      Validators.minLength(5)
    ]);

    this.refCtrl = this._FormBuilder.control('', [
      Validators.required,
      Validators.minLength(4)
    ]);
    this.stateCtrl = this._FormBuilder.control(State.ALIVRER);
    this.form = this._FormBuilder.group({
      name: this.nameCtrl,
      ref: this.refCtrl,
      state: this.stateCtrl
    });
  }

  ngOnInit() {
    /*this.collection = COLLECTION;*/
    this.reset();
  }

  process(): void {
    /*this.dataItem.emit(this.newItem);*/
    /*this.collection.push(this.newItem);*/
    console.log(this.form.value);
    this.dataItem.emit({
      name: this.form.get('name').value,
      reference: this.form.get('ref').value,
      state: this.stateCtrl.value
    });
    this.reset();
  }
  reset(): void {
    /*newItem*/
    /*add to process + add to ngOn Init*/
    /*this.newItem = {
      name: '',
      reference: '',
      state: State.ALIVRER
    };
    */
    this.form.reset();
    this.stateCtrl.setValue(State.ALIVRER);
  }

  isError(champ: string) {
    return this.form.get(champ).invalid && this.form.get(champ).touched;
  }

}
