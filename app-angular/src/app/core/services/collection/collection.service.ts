import { Injectable } from '@angular/core';
import { COLLECTION } from '../../../items/collection';
import { Item } from '../../../shared/interfaces/item.model';

@Injectable()
export class CollectionService {
  collection: Item[];

  constructor() {
    this.collection = this.getCollection();
   }
  getCollection(){
    return this.collection = COLLECTION;
  }

  // add item
  addItem(item: Item) {
    this.collection.push(item);
  }

  update(){}

  delete(){}

}
