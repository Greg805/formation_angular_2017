import { Item } from '../shared/interfaces/item.model';
import { State } from '../shared/enums/state.enum';

export const COLLECTION: Item[] = [
  {
    name: 'Christophe',
    reference: 'ref-1234',
    state: State.ALIVRER
  },
  {
    name: 'Greg',
    reference: 'ref-5678',
    state: State.ENCOURS
  },
  {
    name: 'Sabina',
    reference: 'ref-1235',
    state: State.LIVREE
  }
];
