import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../shared/interfaces/item.model';
import { State } from '../../../shared/enums/state.enum';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
   state = State;
// tslint:disable-next-line:no-input-rename
@Input('itemtoto') item: Item;
  constructor() { }

  ngOnInit() {
  }

}
