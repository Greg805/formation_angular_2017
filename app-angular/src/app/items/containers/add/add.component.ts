import { Component, OnInit } from '@angular/core';
import { Item } from '../../../shared/interfaces/item.model';
import { COLLECTION } from '../../collection';
import { CollectionService } from '../../../core/services/collection/collection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  collection: Item[];

  constructor(private _CollectionService: CollectionService, private _Router: Router) {}

  ngOnInit() {
    this.collection = COLLECTION;
  }
  addItem(item: Item): void {
    // this.collection.push(item);
    this._CollectionService.addItem(item);
    this._Router.navigate(['/liste']);
  }
}
